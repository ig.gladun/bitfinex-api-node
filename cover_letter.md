# Cover letter for the done work during test task

## Overview
I wanted to show a draft of test framework which I think, will be useful in the future and shares my vision what should a good framework have. Few important tests were created which at the moment assures as that the library can work with real environment and provides a very understandable examples how to create other end to end tests. 

## Things I am proud of
* on Gitlab configured caching for node_modules folder. At the moment it saves 10 seconds of the build time. At the moment, it is 15% time saved
* introduced npm config module to use different configurations. This will be useful in future when tests will run locally and on CI. We can have dev config, stagging config, live config, etc. This makes the tests flexible to run against any environment
* introduced REST v1 and REST v2 tests which works with the real environments. Thanks to this tests we will be sure that the library works correctly with real servers and not just with mocks



I wanted to create tests for methods which require authentication, but the system didn't allow me to create API key because of the minimum deposit. http://take.ms/YpMj6 I decided it will take too much time, while you remove the limits and I can start writing the tests. 
All tests are in the `new_test` folder.
One of the difficulties when creating tests is to compare the data received from API vs data from the library. When doing requests, there are a lot of trades and in few milliseconds the data changes and can result as false-positive test failure.

## TODO
* add e2e tests with using real WS2
* add e2e tests for all market data methods
* add e2e tests for personal account data methods
* add e2e tests for all orders and offers methods
* review the README.md file for additional test cases
* migrate existing Mocha tests to tape
* find a way to add tags 


(under e2e (end to end) tests I mean using live or staging environment as a real user would do)
## TODO for the continuous integration
* separate linting from running tests
* run tests on few node versions 
* configure replacement and hiding of credentials
* run tests on few environments


## Running
* `npm install`
* `npm test`  - to run tests with coverage


## Questions
* tape doesn't support test suites (like Mocha's describe). Do we need suites? As a variant, we can use https://www.npmjs.com/package/tape-suite
* I found few UI issues, mostly related to Russian UI. Where should I report them?

