const config = require('config')
const test = require('tape')
const axios = require('axios')

const BFX = require('../index')

test('REST1: Correct ticker is returned for BTCUSD (checking highest ask)', async function (t) {
  const apiKey = config.get('key')
  const apiSecret = config.get('secret')
  const url = config.get('rest1URL')

  const tickerName = 'BTCUSD'

  const bfx = new BFX({
    apiKey: apiKey,
    apiSecret: apiSecret,
    url: url
  })

  const rest1 = bfx.rest(1)
  const actualTicker = await
        rest1.ticker(tickerName)

  const response = await axios.get(`${url}/pubticker/${tickerName}`)
  const expectedTicker = response.data

  console.log(expectedTicker)

  t.deepEqual(actualTicker['high'], expectedTicker[3])
  t.end()
})
