const test = require('tape')

const BFX = require('../index')
const RESTv1 = require('../lib/transports/rest')
const RESTv2 = require('../lib/transports/rest2')

test('BFX should be loaded', function (t) {
  t.equal(typeof BFX, 'function')
  t.end()
})

test('BFX throws on using the deprecated way to set options', function (t) {
  t.throws(() => new BFX(2, {}))
  t.throws(() => new BFX('dummy', 'dummy', 2))
  t.end()
})

test('REST throws an error if invalid version is requested', function (t) {
  const bfx = new BFX()
  t.throws(bfx.rest.bind(bfx, 0))
  t.throws(bfx.rest.bind(bfx, 3))
  t.end()
})

test('REST returns correct REST API version', function (t) {
  const bfx = new BFX()
  const restDefault = bfx.rest()
  const rest1 = bfx.rest(1)
  const rest2 = bfx.rest(2)

  t.ok(restDefault instanceof RESTv2)
  t.ok(rest1 instanceof RESTv1)
  t.ok(rest2 instanceof RESTv2)
  t.end()
})
