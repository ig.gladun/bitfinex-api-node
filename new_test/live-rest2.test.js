const config = require('config')
const test = require('tape')
const axios = require('axios')

const BFX = require('../index')

test('REST1: Correct ticker is returned for BTCUSD (checking highest ask)', async function (t) {
  const apiKey = config.get('key')
  const apiSecret = config.get('secret')
  const url = config.get('rest2URL')

  const tickerName = 'tBTCUSD'

  const bfx = new BFX({
    apiKey: apiKey,
    apiSecret: apiSecret,
    url: url
  })

  const rest2 = bfx.rest(2)
  const actualTicker = await
        rest2.ticker(tickerName)

  const response = await axios.get(`${url}/ticker/${tickerName}`)
  const expectedTicker = response.data

  t.deepEqual(actualTicker[2], expectedTicker[2])// third element of the JSON is the highest ASK
  t.end()
})
