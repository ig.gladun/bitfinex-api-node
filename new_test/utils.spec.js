const test = require('tape')

const utils = require('../lib/util/utils')

// tests for timeFormat()
// expected result is in GMT
test('timeFormat should return correct time when number timestamp is passed', function (t) {
    t.equal(utils.timeFormat(1481112312948), '12:05:12')
    t.end()
})

test('timeFormat should throw exception when string date is passed', function (t) {
    t.equal(utils.timeFormat('December 17, 1995 03:24:00'), '03:24:00')
    t.end()
})

// tests for dateFormat()

test('dateFormat should return correct date by default', function (t) {
    t.equal(utils.dateFormat(1481112312948, '%m-%d-%y %H:%M:%S'), '12-07-2016')
    t.end()
})

test('dateFormat should return correct date for April', function (t) {
    t.equal(utils.dateFormat(1524738618000, '%m-%d-%y %H:%M:%S'), '04-26-2018')
    t.end()
})

test('dateFormat should return correct date from timestamp from format %m-%d-%y %H:%M:%S', function (t) {
    t.equal(utils.dateFormat(1481112312948, '%m-%d-%y %H:%M:%S'), '12-07-2016')
    t.end()
})

test('dateFormat should return correct date from timestamp from format %y-%m-%d %H:%M:%S', function (t) {
    t.equal(utils.dateFormat(1481112312948, '%y-%m-%d %H:%M:%S'), '2016-12-07')
    t.end()
})

// tests for fullDateFormat()

// expected result is in GMT
test('fullDateFormat should return correct date and time for timestamp and  %y-%m-%d %H:%M:%S', function (t) {
    t.equal(utils.fullDateFormat(1481112312948, '%y-%m-%d %H:%M:%S'), '2016-12-07 12:05:12')
    t.end()
})

// expected result is in GMT
test('fullDateFormat should return correct date and time for timestamp and format %m-%d-%y %H:%M:%S', function (t) {
    t.equal(utils.fullDateFormat(1481112312948, '%m-%d-%y %H:%M:%S'), '12-07-2016 12:05:12')
    t.end()
})

// expected result is in GMT
test('fullDateFormat should return correct date for timestamp and time without format', function (t) {
    t.equal(utils.fullDateFormat(1481112312948), '07-12-2016 12:05:12')
    t.end()
})

test('fullDateFormat should return correct date for string variant 1 date and time without format', function (t) {
    t.equal(utils.fullDateFormat('December 17, 1995 03:24:00'), '17-12-1995 03:24:00')
    t.end()
})

test('fullDateFormat should return correct date for string variant 2 date and time without format', function (t) {
    t.equal(utils.fullDateFormat('1995-12-17T03:24:00'), '17-12-1995 03:24:00')
    t.end()
})

test('fullDateFormat should return correct date and time for string variant 1 date  and  %y-%m-%d %H:%M:%S format', function (t) {
    t.equal(utils.fullDateFormat('December 17, 1995 03:24:00', '%y-%m-%d %H:%M:%S'), '1995-12-17 03:24:00')
    t.end()
})

test('fullDateFormat should return correct date and time for string variant 2 date  and  %m-%d-%y %H:%M:%S', function (t) {
    t.equal(utils.fullDateFormat('December 17, 1995 03:24:00', '%m-%d-%y %H:%M:%S'), '12-17-1995 03:24:00')
    t.end()
})

// tests for addDays()
test('addDate return type should be Date', function (t) {
    t.assert(utils.addDays('Thu Dec 03 2016 12:05:12 GMT+0200 (EET)', 1) instanceof Date)
    t.end()
})

test('addDate should return correct date with added days when passing date parameter', function (t) {
    t.equal(utils.addDays('Thu Dec 03 2016 12:05:12 GMT+0200 (EET)', 1).getTime(), new Date('Sun Dec 04 2016 12:05:12 GMT+0200 (EET)').getTime())
    t.end()
})

test('addDate should return correct date with added days when passing negative date (-2) parameter', function (t) {
    t.equal(utils.addDays('Thu Dec 03 2016 12:05:12 GMT+0200 (EET)', -2).getTime(), new Date('Thu Dec 01 2016 12:05:12 GMT+0200 (EET)').getTime())
    t.end()
})

test('addDate should return correct date with added days when passing date parameter and 40 days', function (t) {
    t.equal(utils.addDays('Thu Dec 03 2016 12:05:12 GMT+0200 (EET)', 40).getTime(), new Date('Thu Jan 12 2017 12:05:12 GMT+0200 (EET)').getTime())
    t.end()
})

// tests for round10()

test('round10  should round 5.244444(3) to 5.244', function (t) {
    t.equal(utils.round10(5.244444, 3), '5.244')
    t.end()
})

test('round10  should round 5.244444(0) to 5', function (t) {
    t.equal(utils.round10(5.244444, 0), '5')
    t.end()
})

test('round10  should round 5.244444(0) to 5', function (t) {
    t.equal(utils.round10(5.244444, 0), '5')
    t.end()
})

// tests for floor10()

test('floor10  should floor 5.55(1) to 5.5', function (t) {
    t.equal(utils.floor10(5.55, 1), '5.5')
    t.end()
})

test('floor10  should floor -5.54(1) to -5.6', function (t) {
    t.equal(utils.floor10(-5.54, 1), '-5.6')
    t.end()
})
// tests for ceil10()

test('ceil10  should ceil 5.55(1) to 5.6', function (t) {
    t.equal(utils.ceil10(5.55, 1), '5.6')
    t.end()
})

test('ceil10  should ceil -2000.989823423133523(3) to -2000.984', function (t) {
    t.equal(utils.ceil10(-2000.98423423133523, 3), '-2000.984')
    t.end()
})

// tests for sum()

test('sum  should sum correctly 10,20,30', function (t) {
    t.equal(utils.sum(10, 20, 30), 60)
    t.end()
})

test('sum  should sum correctly zero', function (t) {
    t.equal(utils.sum(0), 0)
    t.end()
})

test('sum  should sum correctly -5, -7, -0.5 , 23', function (t) {
    t.equal(utils.sum(-5, -7, -0.5, 23), 10.5)
    t.end()
})

test('substract  should substract correctly 10,20,30', function (t) {
    t.equal(utils.subtract(10, 20, 30), -40)
    t.end()
})

test('substract  should substract correctly -5, -7, -0.53333, 23', function (t) {
    t.equal(utils.subtract(-5, -7, -0.53333, 23), -20.46667)
    t.end()
})

test('multiply should correctly multiply -5, -0.53333', function (t) {
    t.equal(utils.multiply(-5, -0.53333), 2.66665)
    t.end()
})

test('multiply should correctly multiply 2 and 3', function (t) {
    t.equal(utils.multiply(2, 3), 6)
    t.end()
})

test('divide should correctly divide 2 and 3', function (t) {
    t.equal(utils.divide(2, 3), 0.6666666666666666)
    t.end()
})

test('divide should correctly divide 10 and 5', function (t) {
    t.equal(utils.divide(10, 5), 2)
    t.end()
})

test('divide should throw an error when dividing by zero', function (t) {
    t.throws(utils.divide(10, 0))
    t.end()
})

test('average is calculated correctly for 10, 20, 30', function (t) {
    t.equal(utils.average(10, 20, 30), 20)
    t.end()
})

test('average is calculated correctly for -10, -20, -30', function (t) {
    t.equal(utils.average(-10, -20, -30), -20)
    t.end()
})

test('average is calculated correctly for 1 number', function (t) {
    t.equal(utils.average(6), 6)
    t.end()
})

test('average is calculated correctly for zero', function (t) {
    t.equal(utils.average(0), 0)
    t.end()
})

test('add noise is calculated correctly for arguments (100, 5, 95, 105, 6)', function (t) {
    t.assert(utils.addNoise(100, 5, 95, 105, 6) >= 95)
    t.assert(utils.addNoise(100, 5, 95, 105, 6) <= 105)
    t.end()
})

test('add noise is calculated correctly when min equals to amount + maxdiff', function (t) {
    t.equal(utils.addNoise(100, 5, 105, 106, 6), 105)
    t.end()
})

test('add noise is calculated correctly when  amount + maxdiff is below min', function (t) {
    t.equal(utils.addNoise(100, 5, 106, 107, 3), 106)
    t.end()
})

test('addOrReplaceById should work correctly for 1 replacement', function (t) {
    const L = [{id: 123, value: 'a'}, {id: 456, value: 'bb'}]
    const I = {id: 123, newValue: 'ccc'}
    const result = JSON.stringify([{id: 123, newValue: 'ccc'}, {id: 456, value: 'bb'}])

    t.equal(JSON.stringify(utils.addOrReplaceById(L, I)), result)
    t.end()
})

test('addOrReplaceById should remove duplicates', function (t) {
    const L = [{id: 123, value: 'a'}, {id: 123, value: 'bb'}]
    const I = {id: 123, newValue: 'ccc'}
    const result = JSON.stringify([{id: 123, newValue: 'ccc'}])

    t.equal(JSON.stringify(utils.addOrReplaceById(L, I)), result)
    t.end()
})

// negative tests

test('timeFormat should throw exception when null is passed', function (t) {
    t.throws(utils.timeFormat(null))
    t.end()
})

test('timeFormat should throw exception when undefined is passed', function (t) {
    t.throws(utils.timeFormat(undefined))
    t.end()
})

test('timeFormat should throw exception when negative number is passed', function (t) {
    t.throws(utils.timeFormat(-2000))
    t.end()
})

test('fullDateFormat should throw exception when null is passed', function (t) {
    t.throws(utils.fullDateFormat(null))
    t.end()
})

test('fullDateFormat should throw exception when undefined is passed', function (t) {
    t.throws(utils.fullDateFormat(undefined))
    t.end()
})

test('fullDateFormat should throw exception when float is passed', function (t) {
    t.throws(utils.fullDateFormat(0.2323))
    t.end()
})

test.skip('applyTimeZoneOffset should return something by default', function (t) {
    t.equal(utils.applyTimeZoneOffset(), 'something')
    t.end()
})

test('sum  should sum correctly  array of [10,20,30]', function (t) {
    t.equal(utils.sum([10, 20, 30]), '60')
    t.end()
})

test('sum  should not accept strings with numbers', function (t) {
    t.throws(utils.sum(10, 'abc', 30))
    t.end()
})

test('avarage  should not accept strings with numbers', function (t) {
    t.throws(utils.average(10, 'abc', 30))
    t.end()
})
