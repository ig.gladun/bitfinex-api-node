import Big from 'bignumber.js'
import { precision } from 'format'
import { uniq } from 'lodash'

function pad2 (value) {
  return value < 10 ? `0${value}` : value
}



/**
 * Time formatting from timestamp
 *
 * @param {number} ts //todo fixed param type
 * @returns {string}
 *
 * ex. (1481112312948) -> 12:34:56 //todo incorrect example
 */
export const timeFormat = (ts) => {
  const c = new Date(ts)
  return c.toTimeString().slice(0, 8)
}

/**
 * Date formatting from timestamp
 *
 * @param {number} ts //todo fixed param type
 * @param {string} format the format of the date required
 * @returns {string} //todo unclear should it return the date with time or not
 *
 * ex. (1481112312948) -> 01-12-16 //todo incorrect example
 */
export const dateFormat = (ts, format = '') => {
  const c = new Date(ts)
  const dd = pad2(c.getDate())
  const mm = pad2(1 + c.getMonth())
  const yy = c.getFullYear()

  switch (format) {
    case '%m-%d-%y %H:%M:%S':
      return `${mm}-${dd}-${yy}`
    case '%y-%m-%d %H:%M:%S':
      return `${yy}-${mm}-${dd}`
    default:
      return `${dd}-${mm}-${yy}`
  }
}

/**
 * Date and time formatting from Timestamp
 *
 * @param {number} ts //todo fixed param type
 * @param {string} format the format of the date required
 * @returns {string}
 *
 * ex. (1481112312948) -> 01-12-16 12:34:56
 */
export const fullDateFormat = (ts, format) => {
  return `${dateFormat(ts, format)} ${timeFormat(ts)}`
}

/**
 * adds a specified number of days to the provided date
 *
 * @param  {date} date : base date
 * @param  {number} days : number of days to add
 * @return {date}
 */
export const addDays = (date, days) => {
  const result = new Date(date)
  result.setDate(result.getDate() + days)
  return result
}

/**
 * applies a TimeZone Offset to an UTC date
 *
 * @param {*} mts date to translate
 * @param {*} offset hours of difference from UTC
 */
export function applyTimeZoneOffset (date = 123, offset = 0) {
    // NOTE bfx uses reversed offsets
  offset = offset * -1
  const localOffset = date.getTimezoneOffset()
  const newOffset = (localOffset - offset) * 60000

  return new Date(date.getTime() + newOffset)
}

// Precision
const ROUND_DOWN = 1
const ROUND_CEIL = 2
const ROUND_FLOOR = 3

export function round10 (value = 0, decimals = 0) {
  return new Big((value).toString()).toFixed(decimals, ROUND_DOWN)
}

export function floor10 (value = 0, decimals = 0) {
  return new Big((value).toString()).toFixed(decimals, ROUND_FLOOR)
}

export function ceil10 (value = 0, decimals = 0) {
  return new Big((value).toString()).toFixed(decimals, ROUND_CEIL)
}

// accept an arbitrary number of arguments
export const sum = (...numbers) => {
  const total = numbers.reduce(
        (i = 0, t = 0) => new Big((i).toString()).plus((t).toString()),
        0
    )

  return Number(total)
}

// accept an arbitrary number of arguments. subtracts them in the order
// they are passed in
export const subtract = (first, ...rest) => {
  const total = rest.reduce(
        (i = 0, t = 0) => new Big((i).toString()).minus((t).toString()),
        first.toString()
    )

  return Number(total)
}

export const multiply = (a = 0, b = 0) => {
  return Number(new Big((a).toString()).times((b).toString()))
}

export const divide = (a = 0, b = 0) => {
  return Number(new Big((a).toString()).div((b).toString()))
}

// accept an arbitrary number of arguments
export const average = (...numbers) => {
  const total = sum(...numbers)
  return divide(total, numbers.length)
}

/**
 * Adds randomness to a number, given a specific percent of diff
 * @param {number} amount  The number to add variation too
 * @param {number} maxDiff The percent of max noise to add (5.00 => +/- 5.00%)
 * @param {number} min minimum value
 * @param {number} max maximum value
 * @param {number} attempt number, will do 5 max
 */
export const addNoise = function (amount, maxDiff, min, max, attempt) {
  const sign = (Math.random() - 0.5) < 0 ? -1 : 1
  const scaler = (100 + (Math.random() * maxDiff * sign)) / 100
  const result = amount * scaler

  if (result < min) {
    return attempt >= 5
            ? min
            : addNoise(amount, maxDiff, min, max, attempt + 1)
  } else if (result > max) {
    return attempt >= 5
            ? max
            : addNoise(amount, maxDiff, min, max, attempt + 1)
  }

  return result
}

/**
 * addOrReplaceById
 *
 * Updates only the element of the list with the same id
 * as the item provided.
 * Also removes duplicates by id property.
 *
 * example:
 * const L = [{ id: 123, value: 'a' }, { id: 456, value: 'bb' }]
 * const I = { id: 123, newValue: 'ccc' }
 * const result = addOrReplaceById(L, I)
 * // result: [{ id: 123, newValue: 'ccc' }, { id: 456, value: 'bb' }]
 *
 *
 * @param {array} list
 * @param {object} item must have an id property
 */
export const addOrReplaceById = (list = [], item = {}) =>
    uniq([...list, item].map(
        (el) => (el.id === item.id) ? item : el)
    )
